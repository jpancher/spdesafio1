
export const setVisibilityFilter = filter => ({
  type: 'SET_VISIBILITY_FILTER',
  filter
})

export const showDetail = _id => ({
  type: 'SHOW_DETAIL',
  _id
})

export const filter = word => ({
  type: 'FILTER',
  word
})

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_BRASIL: 'SHOW_BRAZIL'
}

export const fetchCountries = payload => ({
  type: 'FETCH_COUNTRIES',
  payload
})

//export const FETCH_COUNTRIES = 'FETCH_COUNTRIES';