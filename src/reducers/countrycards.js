const countrycards = (state, action) => {
  if (state) {
      console.log('countryCardsReducer',action);

      switch (action.type) {
        case 'SHOW_DETAIL':          
          const clicked = [];
          clicked.push(state.countrycards.find(obj => obj._id === action._id));
          addTop5(clicked[0], state);
          return {
            ...state,
            visiblecc: clicked
          }
        case 'FILTER':
          console.log(state.countrycards.filter(country => country.name.toUpperCase().includes(action.word.toUpperCase())));
        return {
            ...state,
            visiblecc: state.countrycards.filter(country => country.name.toUpperCase().includes(action.word.toUpperCase()))
          }
        case 'FETCH_COUNTRIES_PENDING': 
          return {
              ...state,
              pending: true
            }
        case 'FETCH_COUNTRIES_FULFILLED':
          Transform(action.payload.data.Country);
          //criando array de Location
          const location = [];
          action.payload.data.Country.map(country => location.push({"name": country.name, "location": [country.location.longitude, country.location.latitude]}));
          return {
              ...state,
              pending: false,
              content: '',
              countrycards: action.payload.data.Country,
              visiblecc: action.payload.data.Country,
              location: location
          }
        case 'FETCH_COUNTRIES_ERROR':
          return {
              ...state,
              pending: false,
              error: action.error
          }
        default:
          console.log(state);
          return state
      }
  } else {
    return{ pending:false }
  }
}


const Transform = (arr) => {
  arr.forEach(element => {
    element.flagName = element.flag.svgFile;
    if (element.topLevelDomains[0]!==undefined) {
//      console.log(element.topLevelDomains[0].name);
      element.tld = element.topLevelDomains[0].name;
    } else {
      element.tld = "";
    }
  });
}

function addTop5(element, state) {
  console.log(element, state);
  element.markers = [];
  element.markers.push(  {markerOffset: 0 ,
                    name: element.name,
                    coordinates: [element.location.longitude, element.location.latitude] })
    
                    //top 5 marker
  console.log(element.distanceToOtherCountries[0].countryName);
  console.log(state.location.find(loc => loc.name===element.distanceToOtherCountries[0].countryName));
  console.log(state.location.find(loc => loc.name===element.distanceToOtherCountries[0].countryName).location);

  element.markers.push(  {markerOffset: 0 ,
                      name: element.distanceToOtherCountries[0].countryName,
                      coordinates:
                      state.location.find(loc => loc.name===element.distanceToOtherCountries[0].countryName).location
                     });  
  element.markers.push(  {markerOffset: 0 ,
                      name: element.distanceToOtherCountries[1].countryName,
                      coordinates:
                      state.location.find(loc => loc.name===element.distanceToOtherCountries[1].countryName).location
                     });  
  element.markers.push(  {markerOffset: 0 ,
                      name: element.distanceToOtherCountries[2].countryName,
                      coordinates:
                      state.location.find(loc => loc.name===element.distanceToOtherCountries[2].countryName).location
                     });  
  element.markers.push(  {markerOffset: 0 ,
                      name: element.distanceToOtherCountries[3].countryName,
                      coordinates:
                      state.location.find(loc => loc.name===element.distanceToOtherCountries[3].countryName).location
                     });  
  element.markers.push(  {markerOffset: 0 ,
                      name: element.distanceToOtherCountries[4].countryName,
                      coordinates:
                      state.location.find(loc => loc.name===element.distanceToOtherCountries[4].countryName).location
                     });  
}




export default countrycards