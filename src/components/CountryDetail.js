import React from 'react'
import './CountryDetail.css'
import MapChart from './MapChart'
      
const CountryDetail = ({flagName,name, capital, area, population, tld, markers}) => (

        <div>
                <ul>
                        <li className="CountryDetail"><img src={flagName} alt="Bandeira do País"></img></li>
                        <li>    
                                <h2>{name}</h2>
                                <p>Capital: {capital}</p>
                                <p>Área: {area} km2</p>
                                <p>População: {population}</p>
                                <p>Top Level domain: {tld}</p>
                        </li>
                </ul>
                <MapChart markers={markers}/>
        </div>
)
export default CountryDetail

