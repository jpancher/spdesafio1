import React from 'react'
import PropTypes from 'prop-types'
import CountryCard from './CountryCard'
import CountryDetail from './CountryDetail'

const CountryCardList = ({ countrycards, showDetail}) => {  
  if (countrycards) {
    switch (countrycards.length) {
      case 0:
        return(<h2>Nenhum país encontrado</h2>)
      case 1:
        return (
          <ul>
            <CountryDetail key={countrycards[0]._id} {...countrycards[0]} />
          </ul>
        )
      default:    
          const content = countrycards.map(countrycard => (
          <CountryCard key={countrycard._id} {...countrycard} onClick={() => showDetail(countrycard._id)} />))
          return (
            <ul>
              {content}
            </ul>
          )
    }
  }
  else
  {
    return (
      <h2>Carregando</h2>
    )
  }
}

CountryCardList.propTypes = {
    countrycards: PropTypes.arrayOf(
        PropTypes.shape({
          _id: PropTypes.string.isRequired,
          flagName: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
          capital: PropTypes.string.isRequired,
        })
    ).isRequired,
    showDetail: PropTypes.func.isRequired
}
  
export default CountryCardList
