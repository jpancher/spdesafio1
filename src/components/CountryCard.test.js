import React, { createElement } from 'react'
import CountryCard from './CountryCard'
import CountryDetail from './CountryDetail'
import { render, getByTestId } from '@testing-library/react';

function doNothing() {
    return(0);
}

it('render without crashing', () => {
    const div = document.createElement("div");
    const { getAllByText } = render(<CountryCard onClick={doNothing} flagName={""} name="Brasil" capital="Brasilia" ></CountryCard>, div);

    const linkElement = getAllByText(/Brasil/i);
    expect(linkElement[0]).toBeInTheDocument();
  

//    expect(getByTestId("Brasil")).toHaveTextContent('Brasil');
//    const {getByTestId} = render(<CountryCards onClick={null} flagName={""} name="Brasil" capital="Brasilia" ></CountryCards>)
//  const linkElement = getByText(/brazil/i);
//  expect(linkElement).toBeInTheDocument();
});
