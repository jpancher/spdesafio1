import React from 'react'
import PropTypes from 'prop-types'
import './CountryCard.css'

const CountryCard = ({ onClick, flagName, name, capital }) => (
  <li
    onClick={onClick} 
  >
      <ul className="CountryCard">
              <li><img src={flagName} alt="Baideira do {name}"></img></li>
              <li>    
                  <div>{name}</div>
                  <div>({capital})</div> 
              </li>
      </ul>
  </li>
)

CountryCard.propTypes = {
  onClick: PropTypes.func.isRequired,
  flagName: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  capital: PropTypes.string.isRequired
}

export default CountryCard