import { createStore, combineReducers, applyMiddleware } from 'redux'
//import { createAsyncThunk } from '@reduxjs/toolkit'
//import { logger } from 'redux-logger'
//import math from './reducers/mathReducer'
//import user from './reducers/userReducer'
import cc from './reducers/countrycards'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'

//const store = createStore(    combineReducers({
const store = createStore(
    combineReducers({
        cc
         }),
        {},
        applyMiddleware(thunk, promise)
);

/* const initialValue = {countrycards : [
    {id: 1, flag:"bandeira do brasil", name: "Brasil", capital: "Brasilia"},
    {id: 2, flag: "Bandeira da Inglaterra", name:"Inglaterra", capital:"Londres" },
    {id: 3, flag:"bandeira do japao", name: "Japao", capital: "Tokio"},
    {id: 4, flag:"bandeira da africa do sul", name: "Africa do Sul", capital: "Cape Town, Johanesburgo"},
    {id: 5, flag:"bandeira do canada", name: "Canada", capital: "Ottawa"}
]};
 */

/*  const initialValue = createAsyncThunk('countrycards/fetchCountryCards', async () => {
    try {
        console.log('fetch');
    const res = await fetch('https://countries-274616.ew.r.appspot.com', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: `
        query {
            Country {
                _id
                name
                capital
                flag {
                    svgFile
                }
            }
          }                
      ` }),
      })
      console.log(res);
      return(res.json())
    }
    catch (res) {
        console.log(res.data);
    }
})
 */

//   ceateStore(reducer, state)

export default store;
//export default createStore(rootReducer);
//store.dispatch(countryCardsSlice.fetchCountryCards());