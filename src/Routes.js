import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AppliedRoute from './components/AppliedRoute';
import App from './containers/App';
import CountryDetail from './components/CountryDetail';
import NotFound from './components/NotFound';
//import CountryTop5 from './containers/CountryTop5';

export default ({ myProps }) => (
	<Switch>
		<AppliedRoute path="/" exact component={App} props={myProps} />
		<AppliedRoute path="/countrydetail" component={CountryDetail} props={myProps} />
		{/* Finally, catch all unmatched routes */}
		<Route component={NotFound} />
	</Switch>
);
