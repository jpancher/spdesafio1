import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import App from './containers/App';
import store from './store'
import './index.css';
import * as serviceWorker from './serviceWorker';


/* store.dispatch({
  type: "SHOW",
  payload: 10
}); 
 */

/*  store.dispatch({
  type: "ADD",
  payload: 10
}); 

store.dispatch({
  type: "ADD",
  payload: 15
}); 
 */
// store.dispatch({
//   type: "ADD",
//   payload: 4
// }); 


ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <App />
      </Provider>
    </React.StrictMode>,
  document.getElementById('root')
);



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
