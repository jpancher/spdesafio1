import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux';
import store from '../store';


test('verifica existência do botão Filtrar na página', () => {
  const { getByText } = render(     
    <Provider store={store}>
      <App />
    </Provider>
 );
  const linkElement = getByText(/Filtrar/i);
  expect(linkElement).toBeInTheDocument();
});
