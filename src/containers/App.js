import React, { Component } from 'react';
import './App.css';
import CountryCardList from '../components/CountryCardList';
import {connect} from 'react-redux';
import store from '../store';
import { BrowserRouter } from 'react-router-dom';

//import Routes from '../Routes';

class App extends Component {
  constructor(props) {
    super(props);
    this.filter = this.filter.bind(this);
  }
  
  componentDidMount() {
    console.log(this.props);
    if (!this.props.countrycards && !this.props.pending)
      loadData();
  }

  componentWillUnmount() {
  }

  filter() {
    console.log(this.props);
    console.log(document.getElementById('myfilter').value);
    store.dispatch((dispatch) => {dispatch({type:'FILTER', word: document.getElementById('myfilter').value})});
  }
  
  render() {
    return(
    <BrowserRouter>
      <div className="App">
        <div className="App-header">
          {this.props.content}
          <input type="text" placeholder="Digite sua busca aqui" id="myfilter"></input>
          <button onClick={this.filter}>Filtrar</button>
        </div>
        {this.props.countrycards 
        ? <CountryCardList showDetail={this.props.showDetail} countrycards={this.props.countrycards} />
        : <span>...</span>
        }
      </div>
    </BrowserRouter>    
    );
  }
}

//      <Routes myProps={props} />

function loadData() {
  store.dispatch((dispatch) => {dispatch({type:'FETCH_COUNTRIES', payload: getCountries})});
}


async function getCountries() {
  try {
      console.log('fetch');
    const res = await fetch('https://countries-274616.ew.r.appspot.com', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: `
      query {
          Country {
              _id
              name
              capital
              flag {
                  svgFile
              }
              area
              population
              topLevelDomains {
                name
              }
              location {
                latitude
                longitude
              }          
              distanceToOtherCountries(first: 5) {
                distanceInKm
                countryName
              }
          
          }
        }                
    ` }),
    })
    console.log(res);
    return(res.json())
  }
  catch (res) {
      console.log(res.data);
  }
};


const mapStateToProps = state => {
  return {    
    countrycards: state.cc.visiblecc,
    pending: state.cc.pending,
    content: state.cc.content,
    location: state.cc.location
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showDetail: _id => {
      dispatch({
        type: "SHOW_DETAIL",
        _id: _id })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
