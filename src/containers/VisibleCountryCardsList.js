import { connect } from 'react-redux'
import CountryCardList from './../components/CountryCardList'
import { showDetail } from './../actions/index'
import { VisibilityFilters } from '../actions'


const getVisibleCountryCards = (countrycards, filter) => {
  switch (filter) {
    case 'SHOW_ALL':
      return countrycards
    dafault:
      return countrycards.filter(t => t.name===filter);
  }
}

const mapStateToProps = state => {
  return {    
    countrycards: getVisibleCountryCards(
      state.countrycards, 
      state.visibilityFilter
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showDetail: id => {
      dispatch(showDetail(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CountryCardList)
